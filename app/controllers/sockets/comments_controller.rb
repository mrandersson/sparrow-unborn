module Sockets
  class CommentsController < WebsocketRails::BaseController
    def initialize_session
      
    end

    def create
      new_msg = { headline: message[:headline] }
      broadcast_message 'comments.created', new_msg
    end
  end
end