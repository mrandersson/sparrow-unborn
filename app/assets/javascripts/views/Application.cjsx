React = require('react')
Router = require('react-router')
Reflux = require('reflux')

{RouteHandler} = Router
SideNav = require('./partials/SideNav')
Actions = require('../Actions')

App = React.createClass
  mixins: [
    Router.Navigation,
    Reflux.listenTo(Actions.sessionDestroySuccess, 'handleSessionDestroyed')
  ]

  render: ->
    <div>
      <SideNav/>
      <RouteHandler/>
    </div>

  handleSessionDestroyed: ->
    @replaceWith('/login')

module.exports = App