React = require('react')
CommentField = require('../components/CommentField')
ItemList = require('../components/ItemList')
AuthMixin = require('../mixins/AuthMixin')

Comment = React.createClass
  mixins: [
    AuthMixin
  ]

  render: ->
    <div className="pageComment">
      <h2>Page: Comment</h2>
      <ItemList/>
      <CommentField/>
    </div>

module.exports = Comment
