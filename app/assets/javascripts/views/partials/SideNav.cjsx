React = require('react')
Reflux = require('reflux')
{Link} = require('react-router')

Actions = require('../../Actions')
Auth = require('../../lib/Auth')
CurrentUser = require('../../stores/CurrentUser')

SideNav = React.createClass
  mixins: [
    Reflux.connect(CurrentUser, 'currentUser')
  ]

  getInitialState: ->
    currentUser: {}

  render: ->
    <nav className="sideNav">
      <ul className="sideMenu">
        <li className="sideMenu__item"><Link to="/">Start</Link></li>
        <li className="sideMenu__item"><Link to="comment">Comments</Link></li>
        { if Auth.validSession()
            <li className="sideMenu__item" onClick={@handleLogOut}>Logout ({@state.currentUser.name})</li>
          else
            <li className="sideMenu__item"><Link to="login">Login</Link></li>
        }
      </ul>
    </nav>

  handleLogOut: ->
    Actions.sessionDestroy()

module.exports = SideNav
