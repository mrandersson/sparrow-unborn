React = require('react')
{Navigation} = require('react-router') # Used in handleSgininSuccess @replaceWith
Reflux = require('reflux')

Actions = require('../Actions')

Login = React.createClass
  mixins: [
    Navigation,
    Reflux.listenTo(Actions.sessionCreateSuccess, 'loginSuccess')
  ]

  statics:
    attemptedTransition: null

  render: ->
    <div>
      <h1>New session</h1>
      <form onSubmit={@handleSubmit}>
        <label><input ref="email" placeholder="email" defaultValue="alexander@devil.se"/></label>
        <label><input ref="password" placeholder="password"/></label><br/>
        <button type="submit">login</button>
      </form>
    </div>

  handleSubmit: (e) ->
    e.preventDefault()
    email = @refs.email.getDOMNode().value
    password = @refs.password.getDOMNode().value
    Actions.sessionCreate
        email: email
        password: password
  
  loginSuccess: ->
    if Login.attemptedTransition
      transition = Login.attemptedTransition
      Login.attemptedTransition = null # Reset attempted transition
      transition.retry()
    else
      @replaceWith('/')

module.exports = Login
