React          = require('react')
Reflux         = require('reflux')
_              = require('underscore')

ItemStore      = require('../stores/ItemStore')

ItemList = React.createClass
  mixins: [
    Reflux.connect(ItemStore, 'itemCollection')
  ]

  getInitialState: ->
    itemCollection: ItemStore.collection

  render: ->
    <ul className="itemList">
      { @state.itemCollection.map((item) ->
          <li className="itemList__item" dangerouslySetInnerHTML={{__html: item.get('headline')}}></li>
        )
      }
    </ul>

module.exports = ItemList
