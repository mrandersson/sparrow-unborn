React = require('react')
Medium = require('medium.js')

Actions = require('../Actions')

CommentField = React.createClass

  componentDidMount: ->
    @editor = new Medium(
      element: @refs.editor.getDOMNode()
      mode: Medium.inlineMode
      modifier: 'cmd'
      modifiers:
        'b': 'bold'
    )

  componentWillUnmount: ->
    @editor.destroy()

  render: ->
    <div>
      <div ref="editor" className="wysiwygEditor"></div>
      <button ref="submitButton" onClick={@handleSubmit}>Skicka</button>
    </div>

  handleSubmit: ->
    comment = @editor.value()
    return if comment == ''
    Actions.itemAdd({headline: comment})
    @editor.value('') # Reset editor

module.exports = CommentField
