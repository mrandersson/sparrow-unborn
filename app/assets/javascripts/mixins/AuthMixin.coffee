Auth = require('../lib/Auth')
Login = require('../views/Login')

AuthMixin = 
  statics:
    willTransitionTo: (transition) ->
      unless Auth.validSession()
        Login.attemptedTransition = transition # Save attempted transition so we can redirect to it after login
        transition.redirect('/login')

module.exports = AuthMixin