Backbone = require('backbone')

class Item extends Backbone.Model
  defaults:
    headline: ''
    description: ''

  initialize: ->
    @on 'invalid', (model, errors) ->
      console.error(errors, model)

  validate: (attributes) ->
    errors = []
    for attribute, value of attributes
      errors.push("Unknown attribute: #{attribute}") if @defaults[attribute] == undefined
      errors.push("Wrong type: #{attribute}") if typeof value != typeof @defaults[attribute]
    return errors if errors.length > 0

module.exports = Item
