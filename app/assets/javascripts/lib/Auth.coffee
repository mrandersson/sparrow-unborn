Reflux = require('reflux')
$ = require('jquery')

Actions = require('../Actions')

class Auth
  session: {}
  baseUrl: 'auth'

  constructor: ->
    # Attach Reflux listenerMethods
    for key, value of Reflux.ListenerMethods
      @[key] = value

    @listenTo Actions.sessionCreate, @handleSessionCreate
    @listenTo Actions.sessionDestroy, @handleSessionDestroy
    @fetchFromSessionStorage()

  fetchFromSessionStorage: ->
    session = sessionStorage.getItem('session')
    if session?
      @session = JSON.parse(session)
      Actions.sessionValid() if @validSession()

  handleSessionCreate: (credentials) ->
    $.ajax({
      type: 'POST',
      url: "#{@baseUrl}/sign_in",
      data: credentials
    })
    .done(@onSessionCreateSuccess.bind(@))

  onSessionCreateSuccess: (response, status, xhr) ->
    @session =
      accessToken: xhr.getResponseHeader('access-token')
      client: xhr.getResponseHeader('client')
      expiry: xhr.getResponseHeader('expiry')
      uid: xhr.getResponseHeader('uid')
    sessionStorage.setItem('session', JSON.stringify(@session))
    Actions.sessionCreateSuccess()

  handleSessionDestroy: ->
    @session = {}
    sessionStorage.removeItem('session')
    Actions.sessionDestroySuccess()

  validSession: ->
    (@session.accessToken? && @session.client? && @session.uid? && !@tokenHasExpired())

  tokenHasExpired: ->
    expiry = (parseInt(@session.expiry, 10) * 1000)
    now = new Date().getTime()
    (expiry < now)

module.exports = new Auth()