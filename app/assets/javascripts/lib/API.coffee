$        = require('jquery')
Auth     = require('./Auth')

API = 
  baseUrl: 'api'
  request: (config) ->
    # Performs ajax request with Authorization headers set
    config.beforeSend = (xhr) ->
      xhr.setRequestHeader 'access-token', Auth.session.accessToken
      xhr.setRequestHeader 'token-type', 'Bearer'
      xhr.setRequestHeader 'client', Auth.session.client
      xhr.setRequestHeader 'expiry', Auth.session.expiry
      xhr.setRequestHeader 'uid', Auth.session.uid
    $.ajax(config)

  me: (success, fail) ->
    @request({
      type: 'GET',
      url: "#{@baseUrl}/me"
    })

module.exports = API