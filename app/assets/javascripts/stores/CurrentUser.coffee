Reflux = require('reflux')
API = require('../lib/API')

Actions = require('../Actions')

CurrentUser = Reflux.createStore
  data:
    name: null
    email: null
    nickname: null

  init: ->
    @listenTo Actions.sessionValid, @fetchUser
    @listenTo Actions.sessionCreateSuccess, @fetchUser

  fetchUser: ->
    API.me().done((response, status, xhr) =>
      @data.name = response.name
      @trigger(@data)
    )

module.exports = CurrentUser