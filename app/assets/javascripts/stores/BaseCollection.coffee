Backbone = require('backbone')
Dispatcher = require('../lib/Dispatcher')

class BaseCollection extends Backbone.Collection
  _prepareModel: (attrs, options) ->
    console.log if attrs instanceof @model
    if attrs instanceof @model
      return super if attrs.isValid()
    return false

module.exports = BaseCollection
