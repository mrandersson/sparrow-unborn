Reflux = require('reflux')
Actions = require('../Actions')
Dispatcher = require('../lib/Dispatcher')

BaseCollection = require('./BaseCollection')
Item = require('../models/Item')

# == Collection ==
class ItemCollection extends BaseCollection
  model: Item
  eventNamespace: 'comments'

  initialize: ->
    super
    @add(new Item({headline: 'Lorem ipsum'}))
    @add(new Item({headline: 'Lorem ipsum 2'}))

# == Store ==
ItemStore = Reflux.createStore
  collection: new ItemCollection()

  init: ->
    # Listen to UI events
    @listenTo Actions.itemAdd, @createItem

    # Listen to websocket events
    Dispatcher.bind 'comments.created', @addItem.bind(@)

  # Add item coming from server
  addItem: (item) ->
    # Do nothing, UI already up to date

  # Create item from user
  createItem: (item) ->
    @collection.add(new Item(item))
    @notify()

  # Common function to notify listeners about store change
  notify: ->
    @trigger(Object.freeze(@collection))

module.exports = ItemStore
