jest.dontMock('../../components/CommentField')

describe 'ArticleList', ->
  React = require('react/addons')
  TestUtils = React.addons.TestUtils
  CommentField = require('../../components/CommentField')

  describe 'editor', ->
    it 'is present', ->
      commentField = TestUtils.renderIntoDocument(
        <CommentField />
      )
      expect(commentField.refs.editor).toBeDefined()

  describe 'submit button', ->
    it 'is present', ->
      commentField = TestUtils.renderIntoDocument(
        <CommentField />
      )
      expect(commentField.refs.submitButton).toBeDefined()
