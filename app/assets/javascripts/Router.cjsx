React = require('react/addons')
Router = require('react-router')
{Route, DefaultRoute} = Router

# Main application wrapper
Application = require('views/Application')

# Views
Index = require('views/Index')
Comment = require('views/Comment')
Login = require('views/Login')

routes =
  <Route name="app" path="/" handler={Application}>
    <Route name="login" handler={Login}/>
    <Route name="comment" handler={Comment}/>
    <DefaultRoute handler={Index}/>
  </Route>

window.onload = ->
  Router.run routes, (Handler) ->
    React.render(<Handler/>, document.getElementById('appContainer'))
