Reflux = require('reflux')

Actions = Reflux.createActions([
  'sessionValid'
  'sessionCreate',
  'sessionCreateSuccess',
  'sessionDestroy',
  'sessionDestroySuccess',
  'itemAdd'
])

module.exports = Actions