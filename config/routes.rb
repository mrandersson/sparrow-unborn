Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth', skip: [:omniauth_callbacks]

  namespace :api do
    get '/me' => 'me#show'
  end

  root 'start#index'
end
