require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SparrowUnborn
  class Application < Rails::Application
    config.middleware.delete Rack::Lock
    
    config.assets.paths << Rails.root.join('vendor', 'assets', 'components')

    Rails.application.config.assets.paths << File.join('app', 'components').to_s

    config.browserify_rails.commandline_options = "-t coffee-reactify --extension=\".cjsx\" --extension=\".coffee\""
  end
end
