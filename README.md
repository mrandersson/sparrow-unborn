# Development

### Run the following commands
`bundle`

`nvm use` to set correct version of node. This is needed to run the Jest tests

`npm install`

`bower install`

## Tests
`npm test`